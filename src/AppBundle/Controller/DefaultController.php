<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $day = new \DateTime('next friday');

        $participants = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Participant')
            ->getNextParticipants($day);

        return $this->render('default/index.html.twig', [
            'participants' => $participants,
            'day' => $day
        ]);
    }
}
